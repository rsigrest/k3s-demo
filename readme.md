# k3s demo
This is a demo that uses vagrant to spin up six virtual machines running [k3s](https://k3s.io/) (three servers, three
 workers).

# prerequisites
I ran this demo using libvirt, though it should work fine with VMWare or VirtualBox. For Manjaro, you can install the 
necessary libvirt, qemu, vagrant and kubernetes packages with this command:
```bash
$ sudo pacman -Syyu qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat ebtables iptables vagrant kubectl
```
Next, you'll need to add the vagrant plugin for libvirt with this command:
```bash
$ vagrant plugin install vagrant-libvirt
```

# creating a cluster
From the root directory of this project, run:
```bash
$ vagrant up
```
Wait a bit and it should be running six virutal machines named server1, server2, server3, worker1, worker2 and worker3. 
Unfortunately, the libvirt integration with vagrant is sort of shitty, so `vagrant status` may not be that informative. You
can use `virt-manager` to view the running virtual machines more easily.

# connecting
To connect to the cluster with `kubectl` from your workstation, you'll need to configure `kubectl` by running:
```bash
$ vagrant ssh server1
```
and copying the contents of `/etc/rancher/k3s/k3s.yaml` into your local `~/.kube/config` file, changing "localhost" to the IP
of your server (should be 192.168.42.11). Once this is done, you should be able to run commands like `kubectl describe nodes`
to view cluster information.

# dashboard
To run the kubernetes dashboard, you'll need to create a user and a role in order to get a bearer token to access the UI. Do this
by running the following commands:
```bash
$ kubectl apply -f dashboard-adminuser.yaml
$ kubectl apply -f dashboard-adminuser-role.yaml
```
You can then get a bearer token with this command:
```bash
$ kubectl -n kubernetes-dashboard describe secret admin-user-token | grep ^token
```
Copy the token and run the following commands to deploy the dashboard and create a proxy to it:
```bash
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
$ kubectl proxy
```
Open a browser to [http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/) and sign in with the token.

# running an echoserver
To deploy and expose an echoserver, run the following:
```bash
$ kubectl create deployment hello-node --image=k8s.gcr.io/echoserver:1.4
$ kubectl expose deployment hello-node --type=LoadBalancer --port=8080
```
You should now be able to use the `kubectl get services` command to find the IP address and port (should be 8080) to access the echoserver on from your
browser, curl, insomnia, etc.